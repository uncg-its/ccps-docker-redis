# Welcome
Repository for the CCPS group's work on Docker containers for web servers.

This is a container to run Redis, for caching and storage alongside our PHP/Apache/MySQL applications.

# Getting Started
(Standard Makefile usage is followed. View /Makefile for all available options)
```sh
$ more Makefile
```

#### Copy config

Copy the default configuration file into the mounted folder:

```sh
cp conf-examples/redis_6397.conf mounted-volumes/conf/redis/redis_6397.conf
```

#### BUILD IMAGE
Build the image:
```sh
$ make build-image-no-proxy // for local development
OR
$ make build-image-with-proxy // for remote deployment behind proxy
```
#### DOCKER NETWORK
Create the Docker Network for this instance to run on

Note: you only need to do this if the network isn't already created (ie: if you're also using the PHP/Apache CCPS instance)
```sh
$ make create-network
```
#### RUN/START CONTAINER
Create/run an instance based on that image:
```sh
$ make run-instance
```

That's it. You can then refer to it from other containers by its container name (instance-redis)

# Tagged Version Details

## 1.1
- FROM: redis:5.0
- Explicitly calling `rebuild` during the automated teardown / rebuild scripts, and ensuring that the `--pull` flag is used so that latest version is used

## 1.0.1
- FROM: redis:5.0.3

## 1.0
- FROM: redis
- initial launch
