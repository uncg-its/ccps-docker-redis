FROM redis:5.0
LABEL maintainer="its-laravel-devs-l@uncg.edu"

# Redis config
COPY mounted-volumes/conf/redis/redis_6397.conf /usr/local/etc/redis/redis_6397.conf
# RUN touch /var/log/redis_6397.log
