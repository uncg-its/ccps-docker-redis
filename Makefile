# Cloud Collaboration & Productivity Services, ITS, University of North Carolina at Greensboro

#########################
#						#
#   USAGE				#
#						#
#########################

# Use like any regular *nix Makefile.
# syntax: make <command>
# Example: make build-image-no-proxy

#########################
#						#
#   MAKEFILE VARS		#
#						#
#########################


#########################
#						#
#   DOCKER SPECIFICS	#
#						#
#########################

view-instance-logs:
	docker logs instance-redis

get-instance-ip:
	docker inspect instance-redis | grep "IPAddress"

build-image-no-proxy:
	docker build -t img-redis .

build-image-with-proxy:
	docker build --build-arg http_proxy=http://proxy.uncg.edu:3128 --build-arg https_proxy=http://proxy.uncg.edu:3128 --build-arg HTTP_PROXY=http://proxy.uncg.edu:3128 --build-arg HTTPS_PROXY=http://proxy.uncg.edu:3128 -t img-redis .

rebuild-image-no-proxy:
	docker build --pull --no-cache -t img-redis .

rebuild-image-with-proxy:
	docker build --pull --no-cache  --build-arg http_proxy=http://proxy.uncg.edu:3128 --build-arg https_proxy=http://proxy.uncg.edu:3128 --build-arg HTTP_PROXY=http://proxy.uncg.edu:3128 --build-arg HTTPS_PROXY=http://proxy.uncg.edu:3128 -t img-redis .

remove-image:
	docker rmi img-redis

create-network:
	docker network create --driver bridge ccps_docker_network

# NOTE: instance may need to be restarted again if the local folders do not exist already before the first run.
run-instance:
	docker run -d \
	-p 6379:6379 \
	-e TZ=America/New_York \
	--name instance-redis \
	-v $(CURDIR)/mounted-volumes/data:/data/:Z \
	--restart always \
	--network=ccps_docker_network \
	img-redis \
	redis-server

restart-instance:
	docker restart instance-redis

stop-instance:
	docker stop instance-redis

stop-remove-instance:
	docker stop instance-redis
	docker rm instance-redis

login-to-instance:
	sudo docker exec -i -t instance-redis /bin/bash

#########################
#						#
#   MAINTENANCE SETS	#
#						#
#########################

script-rebuild-tear-down-restart-no-proxy:
	make build-image-no-proxy
	make stop-remove-instance
	make run-instance

script-rebuild-tear-down-restart-with-proxy:
	make build-image-with-proxy
	make stop-remove-instance
	make run-instance

prune-images:
	docker image prune --force

#########################
#						#
#   UNCG HELPERS		#
#						#
#########################

export-uncg-proxy:
	export https_proxy="proxy.uncg.edu:3128" && export http_proxy="proxy.uncg.edu:3128" && export HTTP_PROXY="proxy.uncg.edu:3128" && export HTTPS_PROXY="proxy.uncg.edu:3128"
